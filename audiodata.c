#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <alsa/asoundlib.h>
#include <math.h>
#include <stdlib.h>
#include <strings.h>

#include <fftw3.h>





#define TRUE 1
#define FALSE 0

#define NUMBINS 4096

extern snd_pcm_t *capture_handle;


#define RATE 44100   /* the sampling rate */
#define SIZE 16      /* sample size: 8 or 16 bits */
#define CHANNELS 1  /* 1 = mono 2 = stereo */








#define WINDOW(j,a,b) (1.0-fabs((((j)-1)-(a))*(b))) /* Bartlett */


int LENGTH; // set in main()

int has_zero;
double amp_min[2048];

static double dbvals[NUMBINS/2],ampvals[NUMBINS/2];;
double xvals[NUMBINS/2],yvals[NUMBINS/2];



/***
get audio buffer for 60sec@44100
get fftw buffers for up to 4096 bins
***/

static fftw_complex    *data, *fft_result;//, *ifft_result;
static fftw_plan       plan_forward;//, plan_backward;
char * buf;

void ffmem_alloc()
    {
    if(!(buf = malloc(60*RATE*SIZE*CHANNELS/8)))
        {
        printf("no mem @ %d,%s\n",__LINE__,__FILE__);
        exit(1);
        }
    data        = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * 4096 );
    fft_result  = ( fftw_complex* ) fftw_malloc( sizeof( fftw_complex ) * 4096 );
    plan_forward  = fftw_plan_dft_1d( 4096, data, fft_result, FFTW_FORWARD, FFTW_ESTIMATE );



    }

void ffmem_free()
    {
    free(buf);
    fftw_free(data);
    fftw_free(fft_result);
    }

void
do_sample(int numbins,int dur)
    {
    int status;   /* return status of system calls */
    double R,I,A,P,F0;
    int N;
    uint16_t *p;
    int numruns, maxruns;

    int             i;
    int out;
    double tmp;

    out = open("audio.raw",O_WRONLY|O_CREAT);

    status = read_samples(dur ? (LENGTH*RATE): numbins,buf, capture_handle); /* record some sound */
    write(out,buf,dur ? (LENGTH*RATE): numbins);
    p = (uint16_t *)buf;


    /******************************************************************/
    F0 = (double)RATE/(double)numbins;
    N = numbins;
    bzero(&dbvals,numbins/2);
    bzero(&ampvals,numbins/2);

    bzero(data, sizeof( fftw_complex ) * numbins );
    bzero(fft_result,  sizeof( fftw_complex ) * numbins );



    maxruns = dur ? (LENGTH*RATE)/numbins : 1;


    printf("maxruns = %d\n",maxruns);
    for(numruns =0; numruns != maxruns; numruns++)
        {
        /*************************************************/
        for(i =0 ; i != numbins; i++)
            {
            data[i][0] = p[i+(numruns*(numbins))];
            data[i][1] = 0.0;
            }




        for (i=0; i<=numbins; i++)
            {
            // Apply the window to the data.
            data[i][0] *=  WINDOW(i+1,(double)NUMBINS/2,(double)1.0/(NUMBINS/2));


            }

       /**** call ff/dft *******/

        fftw_execute( plan_forward );

        /***********************************************/

        for(i = 1; i != (N/2)+1 ; i++)
            {

            R = fft_result[i][0];
            I = fft_result[i][1];
            //F = F0 * (double)i;
            A = sqrt( (R*R) + (I*I));
            P = atan2(I,R);
            if(ampvals[i-1] < A)
                {
                ampvals[i-1] = A;
                }


            // de amplitude: printf("i=%d -> %d\n",i,(int)sqrt( fft_result[i][0]*fft_result[i][0] + fft_result[i][1]*fft_result[i][1] ));
            // (in dbm) agnitude_dB = 20*log10(magnitude))
            // phase = atan2(imag,real)
            // frequency spacing of bins: sampleRate/N
            // R = A*cos(P) and I = A*sin(P)
            }

        }



    if(0)//has_zero == 1)
        {
        tmp = 0.0;
        for(i = 0; i != numbins/2; i++)
            {

            if(ampvals[i] > tmp)
                tmp = ampvals[i];
            }

        for(i = 1; i != (numbins/2)+1; i++)
            {

            //yvals[i-1] =  (ampvals[i] - amp_min[i])/1024.0; // db view sucks :-)
            yvals[(i-1)] = 20*log10(ampvals[i]/tmp);// - 58.8;///amp_min[i]);// - 53.0;
            xvals[(i-1)] = F0*(double)(i);
            }
        // 'pad' array, were not looking at DC
        yvals[2047] = yvals[2046];
        tmp = yvals[0];
        for(i = 1; i != numbins/2; i++)
            {
            if(yvals[i] < tmp)
                tmp = yvals[i];
            }

        // 'pad
        printf("min %f\n",tmp);
        }
    else // absolute values
        {

        for(i = 1; i != (numbins/2)+1; i++)
            {
            xvals[i-1] = F0*(double)i;
            yvals[i-1] = ampvals[i]/1024.0;

            }
        }

    }

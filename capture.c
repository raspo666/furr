#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <alsa/asoundlib.h>


#define CAP_CHANS 1
#define PLAY_CHANS 2

 snd_pcm_t *capture_handle;

  snd_pcm_t *
  open_cap(char *device)
{
  int i;
  int err;
  unsigned int rate;
  short buf[128];
  snd_pcm_t *capture_handle;
  snd_pcm_hw_params_t *hw_params;

  if ((err = snd_pcm_open (&capture_handle, device, SND_PCM_STREAM_CAPTURE, 0)) < 0)
    {
      fprintf (stderr, "cannot open audio device %s (%s)\n",
	       device,
	       snd_strerror (err));
      exit (1);
    }

  if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0)
    {
      fprintf (stderr, "cannot allocate hardware parameter structure (%s)\n",
	       snd_strerror (err));
      exit (1);
    }

  if ((err = snd_pcm_hw_params_any (capture_handle, hw_params)) < 0)
    {
      fprintf (stderr, "cannot initialize hardware parameter structure (%s)\n",
	       snd_strerror (err));
      exit (1);
    }

  if ((err = snd_pcm_hw_params_set_access (capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
    {
      fprintf (stderr, "cannot set access type (%s)\n",
	       snd_strerror (err));
      exit (1);
    }

  if ((err = snd_pcm_hw_params_set_format (capture_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0)
    {
      fprintf (stderr, "cannot set sample format (%s)\n",
	       snd_strerror (err));
      exit (1);
    }
	rate = 44100;
  if ((err = snd_pcm_hw_params_set_rate_near (capture_handle, hw_params, &rate, 0)) < 0)
    {
      fprintf (stderr, "cannot set sample rate (%s)\n",
	       snd_strerror (err));
      exit (1);
    }

  if ((err = snd_pcm_hw_params_set_channels (capture_handle, hw_params, CAP_CHANS)) < 0)
    {
      fprintf (stderr, "cannot set channel count (%s)\n",
	       snd_strerror (err));
      exit (1);
    }

  if ((err = snd_pcm_hw_params (capture_handle, hw_params)) < 0)
    {
      fprintf (stderr, "cannot set parameters (%s)\n",
	       snd_strerror (err));
      exit (1);
    }

  snd_pcm_hw_params_free (hw_params);

  if ((err = snd_pcm_prepare (capture_handle)) < 0) {
    fprintf (stderr, "cannot prepare audio interface for use (%s)\n",
	     snd_strerror (err));
    exit (1);
  }

return capture_handle;
}

int
read_samples(uint32_t num,char *buf,snd_pcm_t *capture_handle)
{
  int err;
  printf("read %d to %x handle=%x\n",num,buf,capture_handle);
while(snd_pcm_prepare(capture_handle) < 0)
   ; //printf("prepare failed\n");

if(snd_pcm_reset(capture_handle) < 0)
  printf("reset failed\n");
memset(buf,0,num);
if ((err = snd_pcm_readi (capture_handle, buf, num) != num))
	{
	  fprintf (stderr, "read from audio interface failed (%s)\n",
		   snd_strerror (err));
	  return 0 ;
	}
return 1;
}


void cap_close(snd_pcm_t *capture_handle)
{
  snd_pcm_close (capture_handle);
  exit (0);
}

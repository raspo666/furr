CFLAGS = -O2 -Wall 
DEFS = -D_XOPEN_SOURCE -D_BSD_SOURCE -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE
LIBS = -lfftw3 -lm -lforms -lasound 
#-L /usr/local/lib/mysql 
CC = gcc

#LIBDIRS= -L wherever

DEPS  = Makefile
SRCS = audiodata.c furr_form.c  callbacks.c  main.c capture.c
OBJS = audiodata.o furr_form.o  callbacks.o  main.o capture.o


obj/%.o: %.c $(DEPS)
	$(CC) $(DEFS) $(CFLAGS)  -c $<  -o $@

#obj/%.o: epg/%.cpp 
#	$(CC) $(DEFS) $(CFLAGS)  -c $<  -o $@


v2Yahdr: $(OBJS) $(DEPS)
	$(CC) -o furr $(OBJS) $(LIBDIRS) $(LIBS)





clean: 
	rm *.o furr

reallyclean:
	clean
	rm *~
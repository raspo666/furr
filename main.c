#include "furr_form.h"
#include <alsa/asoundlib.h>

extern snd_pcm_t *capture_handle;

FL_OBJECT * txt_pos;

FD_furr *fd_furr;

extern double xvals[],yvals[];

extern int LENGTH;

int
main( int    argc,
      char * argv[ ] )
    {

    LENGTH = 1;


    capture_handle = open_cap("plughw:1,0");
    ffmem_alloc();

    fl_initialize( &argc, argv, 0, 0, 0 );
    fd_furr = create_form_furr( );

    txt_pos = fd_furr->txt_pos;
    /* Fill-in form initialization code */

    //fl_set_xyplot_xgrid(fd_furr->pl_data, FL_GRID_MAJOR);
    //fl_set_xyplot_xgrid(fd_furr->pl_data, FL_GRID_MINOR);
    fl_set_slider_bounds(fd_furr->sl_stime, 1.0, 60.0);


    fl_set_xyplot_inspect(fd_furr->pl_data, 1);

    fl_set_xyplot_xtics(fd_furr->pl_data, 100, 1);


    fl_set_xyplot_data_double(fd_furr->pl_data, xvals, yvals, 2048,
                              "FFT", "Freq",
                              "Amp");


    /* Show the first form */

    fl_show_form( fd_furr->furr, FL_PLACE_CENTERFREE, FL_FULLBORDER, "furr" );

    fl_do_forms( );

    if ( fl_form_is_visible( fd_furr->furr ) )
        fl_hide_form( fd_furr->furr );
    fl_free( fd_furr );
    fl_finish( );
    cap_close(capture_handle);
    ffmem_free();
    return 0;
    }

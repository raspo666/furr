#include "furr_form.h"

/* Callbacks and freeobj handles for form furr */


extern  FD_furr *fd_furr;


/***************************************
 ***************************************/


// these are always 2048




extern double xvals[],yvals[];

void bt_sample_cb( FL_OBJECT * ob,
         long        data )
{
    /* Fill-in code for callback here */
  
  fl_set_object_label(ob,"Sampling");
  fl_redraw_object(ob);
  do_sample();
  fl_set_xyplot_data_double(fd_furr->pl_data, xvals, yvals, 2048,
			    "FFT", "Freq",
			    "Amp");
  fl_set_object_label(ob,"Sample");
fl_redraw_object(ob);

}


/***************************************
 ***************************************/

void bt_zero_cb( FL_OBJECT * ob,
         long        data )
{
    /* Fill-in code for callback here */
}


/***************************************
 ***************************************/

void bt_quit_cb( FL_OBJECT * ob,
         long        data )
{
    /* Fill-in code for callback here */
  // :-)
  exit(0);

}


/***************************************
 ***************************************/

void bt_save_cb( FL_OBJECT * ob,
         long        data )
{
    /* Fill-in code for callback here */
  char *fname;

  fname = fl_get_input(fd_furr->ip_name);
  if(strlen(fname) == 0)
    {
      
    }


}


/***************************************
 ***************************************/


extern FL_OBJECT * txt_pos;


void pl_data_cb( FL_OBJECT * ob,
         long        data )
{
    /* Fill-in code for callback here */
  float x,y;
  int n;
  char buffer[255];

  n = 1;
  fl_get_xyplot(ob, &x, &y, &n);
  sprintf(buffer, "Freq = %5.3f Hz\n",x);
  fl_set_object_label(txt_pos,buffer);
 

}





#include "furr_form.h"

int
main( int    argc,
      char * argv[ ] )
{
    FD_furr *fd_furr;

    fl_initialize( &argc, argv, 0, 0, 0 );
    fd_furr = create_form_furr( );

    /* Fill-in form initialization code */


    /*    fl_set_xyplot_file(fd_furr->pl_data, "testdata",
                       "hans ","freq",
                       "level");*/

    fl_set_xyplot_xtics(fd_furr->pl_data, 100, 1);
    fl_set_xyplot_xgrid(fd_furr->pl_data, 200);
    /* Show the first form */

    fl_show_form( fd_furr->furr, FL_PLACE_CENTERFREE, FL_FULLBORDER, "furr" );

    fl_do_forms( );

    if ( fl_form_is_visible( fd_furr->furr ) )
        fl_hide_form( fd_furr->furr );
    fl_free( fd_furr );
    fl_finish( );

    return 0;
}

#include <stdlib.h>
#include <stdio.h>
#include "furr_form.h"
#include <pthread.h>

/* Callbacks and freeobj handles for form furr */


extern  FD_furr *fd_furr;
extern int LENGTH;
static pthread_t updater;
static int islive;

/***************************************
 ***************************************/


// these are always 2048
extern double xvals[],yvals[];

extern int  has_zero;
extern double amp_min[2048];

extern void do_sample();

static int pos;
static double min,max;
static int iscompact;

void bt_sample_cb( FL_OBJECT * ob,
                   long        data )
    {

    int i;
    fl_set_object_label(ob,"Sampling");
    fl_redraw_object(ob);
    do_sample(4096,1);
    pos = 0;
    max = 0.0;
    min = 0.0;

    for(i = 0; i != 2048; i++)
        {
        if(yvals[i] > max)
            max = yvals[i];
        }
    fl_set_xyplot_ybounds(fd_furr->pl_data, min,  max);
    fl_set_xyplot_data_double(fd_furr->pl_data, &xvals[pos], &yvals[pos], 256,
                              "FFT", "Freq",
                              "Amp");
    pos = 256;

    fl_set_object_label(ob,"Sample");
    fl_redraw_object(ob);
    iscompact = 0;
    }

void bt_fw_cb( FL_OBJECT * ob,
               long        data )
    {
    /* Fill-in code for callback here */
    if(pos < (2048-256))
        {
        pos += 256;
        fl_set_xyplot_ybounds(fd_furr->pl_data, min,  max);
        fl_set_xyplot_data_double(fd_furr->pl_data, &xvals[pos], &yvals[pos], 256,
                                  "FFT", "Freq",
                                  "Amp");
        }

    }

void bt_rev_cb( FL_OBJECT * ob,
                long        data )
    {
    /* Fill-in code for callback here */

    if(pos >= 256)
        {
        pos -= 256;
        fl_set_xyplot_ybounds(fd_furr->pl_data, min,  max);
        fl_set_xyplot_data_double(fd_furr->pl_data, &xvals[pos], &yvals[pos], 256,
                                  "FFT", "Freq",
                                  "Amp");
        }


    }












void bt_compact_cb( FL_OBJECT * ob,
                    long        data )
    {
    /* Fill-in code for callback here */


    if(iscompact == 0)
        {
        pos = 0;
        fl_set_xyplot_ybounds(fd_furr->pl_data, min,  max);
        fl_set_xyplot_data_double(fd_furr->pl_data, &xvals[0], &yvals[0], 2048,
                                  "FFT", "Freq",
                                  "Amp");
        iscompact = 1;
        }
    else
        {
        fl_set_xyplot_ybounds(fd_furr->pl_data, min,  max);
        fl_set_xyplot_data_double(fd_furr->pl_data, &xvals[pos], &yvals[pos], 256,
                                  "FFT", "Freq",
                                  "Amp");
        pos = 256;
        iscompact = 0;
        }

    }







/***************************************
 ***************************************/

void sl_stime_cb( FL_OBJECT * ob,
                  long        data )
    {
    /* Fill-in code for callback here */



    LENGTH = fl_get_slider_value( ob );
    //  printf("len = %d\n",LENGTH);


    }









/***************************************
 ***************************************/

void bt_quit_cb( FL_OBJECT * ob,
                 long        data )
    {
    /* Fill-in code for callback here */
    // :-)
    exit(0);

    }


/***************************************
 ***************************************/

void bt_save_cb( FL_OBJECT * ob,
                 long        data )
    {
    /* Fill-in code for callback here */
    const char *fname;
    FILE *out;
    int i;

    fname = fl_show_input("Enter filename", "audio.dat");
    if(strlen(fname) == 0)
        {
        fl_show_alert("Cannot save without name", "", "",
                      1);
        return;
        }
    if(!(out = fopen(fname,"w")))
        {
        fl_show_alert("File open failed !", "try another name.", "",
                      1);
        return;
        }
    for(i = 0; i != 2048; i++)
        {
        fprintf(out,"%f\t%f\n",xvals[i],yvals[i]);
        }
    fclose(out);

    }


/***************************************
 ***************************************/


extern FL_OBJECT * txt_pos;


void pl_data_cb( FL_OBJECT * ob,
                 long        data )
    {
    /* Fill-in code for callback here */
    float x,y;
    int n;
    char buffer[255];

    n = 1;
    fl_get_xyplot(ob, &x, &y, &n);
    sprintf(buffer, "Freq = %5.3f Hz / %3.3f A\n",x,y);
    fl_set_object_label(txt_pos,buffer);


    }




